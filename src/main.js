import Vue from "vue";
import App from "./App.vue";
import { router } from "./router";
import store from "./store/index";
import ApiService from "./services/api.service";
Vue.config.productionTip = false;
ApiService.init("//192.168.10.10/api/");

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
