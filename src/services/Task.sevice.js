import ApiService from "./api.service";
import store from "../store/index";

export default class TaskService {
  getTasks() {
    return ApiService.get("tasks").then(data => {
      store.commit("setTasks", data.data);
    });
  }

  getTask(id) {
    return ApiService.get("tasks/" + id).then(data => {
      store.commit("setTask", data.data);
    });
  }

  editTask(id, task) {
    return ApiService.put("tasks/" + id, task);
  }

  addTasks(title, description, userId) {
    return ApiService.post("tasks", { title, description, userId });
  }

  getNotes() {
    return ApiService.get("notes").then(data => {
      store.commit("setNotes", data.data);
    });
  }

  getNote(id) {
    return ApiService.get("notes/" + id).then(data => {
      store.commit("setNote", data.data);
    });
  }

  addNote(text, taskId) {
    return ApiService.post("notes", { text, taskId });
  }

  editNote(id, note) {
    return ApiService.put("notes/" + id, note);
  }
}

export const taskService = new TaskService();
