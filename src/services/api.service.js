import axios from "axios";
import { TokenService } from "./storage.service";

const axiosInstance = axios.create({
  headers: {
    common: {
      Accept: "application/json"
    },
    post: {
      Accept: "application/json"
    }
  }
  /* other custom settings */
});

const ApiService = {
  init(baseURL) {
    axiosInstance.defaults.baseURL = baseURL;
  },

  setHeader() {
    axiosInstance.defaults.headers.common[
      "Authorization"
    ] = `Bearer ${TokenService.getToken()}`;
  },

  removeHeader() {
    axiosInstance.defaults.headers.common = {};
  },

  get(resource) {
    return axiosInstance.get(resource);
  },

  post(resource, data) {
    return axiosInstance.post(resource, data);
  },

  put(resource, data) {
    return axiosInstance.put(resource, data);
  },

  delete(resource) {
    return axiosInstance.delete(resource);
  },
  customRequest(data) {
    return axiosInstance(data);
  }
};

export default ApiService;
