import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Login from "./components/Login.vue";
import Register from "./components/Register.vue";
import Tasks from "./components/Tasks.vue";
import SingleTask from "./components/SingleTask.vue";
import SingleNote from "./components/SingleNote.vue";

const routes = [
  {
    path: "/login",
    component: Login,
    name: "login"
  },
  {
    path: "/register",
    component: Register,
    name: "register"
  },
  { path: "/tasks", component: Tasks, name: "tasks" },
  {
    path: "/tasks/:id",
    component: SingleTask,
    name: "singleTask"
  },
  {
    path: "/notes/:id",
    component: SingleNote,
    name: "singleNote"
  }
];

export const router = new VueRouter({
  routes: routes,
  mode: "history"
});
