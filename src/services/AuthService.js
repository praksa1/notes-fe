import ApiService from "./api.service";
import { TokenService } from "./storage.service";
import store from "../store/index";

export default class AuthService {
  login(email, password) {
    return ApiService.post("login", { email, password })
      .then(data => {
        TokenService.saveToken(data.data.token);
        ApiService.setHeader();
        store.commit("setUser", data.data.user);
        window.localStorage.setItem("access_token", data.data.token);

        window.localStorage.setItem("user", JSON.stringify(data.data.user));
      })
      .catch(e => {
        alert(e);
      });
  }

  logout() {
    ApiService.removeHeader();
    TokenService.removeToken();
  }

  register(newUser) {
    return ApiService.post("register", newUser);
  }

  isAuthenticated() {
    return !!window.localStorage.getItem("token");
  }
}

export const authService = new AuthService();
