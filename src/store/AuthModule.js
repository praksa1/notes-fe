import { authService } from "./../services/AuthService";

const user = JSON.parse(localStorage.getItem("user")) || null;
const tasks = {};
const notes = {};
const task = {};
const note = {};

export const AuthModule = {
  namspaced: true,

  state: {
    user,
    tasks,
    notes,
    task,
    note
  },

  mutations: {
    setUser(state, payload) {
      state.user = payload;
    },
    setTasks(state, payload) {
      state.tasks = payload;
    },
    setNotes(state, payload) {
      state.notes = payload;
    },
    setTask(state, payload) {
      state.task = payload;
    },
    setNote(state, payload) {
      state.note = payload;
    }
  },

  actions: {
    login(context, { email, password }) {
      authService.login(email, password);
    },

    register(context, newUser) {
      authService.register(newUser);
    }
  },

  getters: {
    getUser(state) {
      return state.user;
    },
    getTasks(state) {
      return state.tasks;
    },
    getNotes(state) {
      return state.notes;
    },
    getTask(state) {
      return state.task;
    },
    getNote(state) {
      return state.note;
    }
  }
};
